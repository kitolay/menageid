import axios, { AxiosRequestConfig } from 'axios';

const base_api_url = 'http://localhost:3000/'

export default class Http {
  get(url: string, config?: AxiosRequestConfig):Promise<any>{
    return axios.get(base_api_url+url, config)
  }

  post(url: string, data?: any):Promise<any>{
    return axios.post(base_api_url+url,data)
  }
}
