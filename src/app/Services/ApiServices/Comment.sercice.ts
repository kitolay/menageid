import Http from "./Http"

const http = new Http()

export function authentUser(username: string, userpass: string): Promise<boolean> {
  return http.post('authent',{
    uname: username,
    upass: userpass
  })
}

export function connextionTest():Promise<any> {
  return http.get('timezones')
}
